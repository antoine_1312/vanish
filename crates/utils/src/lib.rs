use rand::{thread_rng, Rng};

pub fn generate_random_numeric_string(size: usize) -> String {
    const CHARSET: &[u8] = b"0123456789";
    (0..size)
        .map(|_| {
            let idx = thread_rng().gen_range(0..CHARSET.len());
            CHARSET[idx] as char
        })
        .collect()
}

pub fn char_digit_to_u8(c: char) -> i8 {
    let array: [i8; 10] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let res: Vec<&i8> = array
        .iter()
        .filter(|v| v.to_string() == c.to_string())
        .collect();

    *res[0]
}