/*

Vanisher

Made by Coldwire for the Vanish Project

Based on the "blue-merle" projet by srlabs:
https://github.com/srlabs/blue-merle/blob/main/files/lib/blue-merle/imei_generate.py

*/

use clap::{Parser, Subcommand};

use imei;

/// A fictional versioning CLI
#[derive(Debug, Parser)] // requires `derive` feature
#[command(name = "vanisher")]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    Generate,
    #[command(arg_required_else_help = false)]
    Set {
        imei: String,
    },
    GetIMEI,
    GetIMSI,
}

fn main() {
    let args = Cli::parse();

    match args.command {
        Commands::Generate => {
            println!("Generated IMEI: {}", imei::generate());
        }
        Commands::Set { imei } => {
            let modem = at::modems::init("quectel_ep06");
            match modem.set_imei(&imei) {
                Ok(r) => println!("{}", r),
                Err(e) => println!("{}", e),
            };
        }
        Commands::GetIMEI => todo!(),
        Commands::GetIMSI => todo!(),
    }
}
