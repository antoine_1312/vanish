/*

Based on the "blue-merle" projet by srlabs
https://github.com/srlabs/blue-merle/blob/main/files/lib/blue-merle/imei_generate.py

*/

use rand::seq::SliceRandom;
use utils;

const IMEI_LENGTH: i8 = 14; // without validation digit
const IMEI_PREFIX: [&str; 16] = [
    "35674108", "35290611", "35397710", "35323210", "35384110", "35982748", "35672011", "35759049",
    "35266891", "35407115", "35538025", "35480910", "35324590", "35901183", "35139729", "35479164",
];

pub fn generate() -> String {
    let mut result_imei: String = "".to_owned();

    // We choose a random prefix from the predefined list.
    match IMEI_PREFIX.choose(&mut rand::thread_rng()) {
        Some(i) => result_imei.push_str(&i),
        None => todo!(),
    };

    // Then we fill the rest with random characters
    let random_part_length: i8 = IMEI_LENGTH - result_imei.len() as i8;
    let random_part = utils::generate_random_numeric_string(random_part_length as usize);
    result_imei.push_str(&random_part);

    /* calculate validation digit
    Double each second digit in the IMEI: 4 18 0 2 5 8 2 0 3 4 3 14 5 2
    (excluding the validation digit) */
    let mut iteration_1: String = "".to_owned();
    for (i, c) in result_imei.chars().enumerate() {
        if i % 2 == 0 {
            iteration_1.push(c);
        } else {
            let double_digit: i8 = 2 * utils::char_digit_to_u8(c);
            iteration_1.push_str(&i8::to_string(&double_digit));
        }
    }

    /* Separate this number into single digits: 4 1 8 0 2 5 8 2 0 3 4 3 1 4 5 2
    (notice that 18 and 14 have been split).
    Add up all the numbers: 4+1+8+0+2+5+8+2+0+3+4+3+1+4+5+2 = 52 */
    let sum = iteration_1
        .chars()
        .collect::<Vec<char>>()
        .iter()
        .fold(0, |a, b| a + utils::char_digit_to_u8(*b));

    /* Take your resulting number, remember it, and round it up to the nearest
    multiple of ten: 60.
    Subtract your original number from the rounded-up number: 60 - 52 = 8. */
    let mut validation_digit = (sum - (sum % 10) + 10) - sum;
    if validation_digit >= 10 {
        validation_digit = 0
    }

    result_imei.push_str(&validation_digit.to_string());

    result_imei
}
