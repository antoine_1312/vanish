use std::time::Duration;

use serialport::SerialPortBuilder;

use crate::utils;

use super::ModemActions;

pub struct QuectelEp06 {
    pub connection: SerialPortBuilder,
}

impl ModemActions for QuectelEp06 {
    fn get_imei(&self) -> Result<String, String> {
        let cmd = "AT+CGSN".to_string();

        match utils::send_serial_command(self.connection.clone(), cmd) {
            Ok(r) => Ok(r),
            Err(_) => todo!(),
        }
    }

    fn get_imsi(&self) -> Result<String, String> {
        let cmd = "".to_string();

        match utils::send_serial_command(self.connection.clone(), cmd) {
            Ok(r) => Ok(r),
            Err(_) => todo!(),
        }
    }

    fn set_imei(&self, imei: &str) -> Result<String, String> {
        let cmd = format!("AT+EGMR=1,7,\"{}b'\"", imei);

        match utils::send_serial_command(self.connection.clone(), cmd) {
            Ok(r) => Ok(r),
            Err(_) => todo!(),
        }
    }
}
