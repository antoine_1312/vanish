use std::time::Duration;

use serialport::SerialPortBuilder;

use self::quectel_ep06::QuectelEp06;

mod quectel_ep06;

pub trait ModemActions: Send {
    fn get_imei(&self) -> Result<String, String>;
    fn get_imsi(&self) -> Result<String, String>;
    fn set_imei(&self, imei: &str) -> Result<String, String>;
}

pub fn init(model: &str) -> Box<dyn ModemActions> {
    let base_port: SerialPortBuilder = serialport::new("", 0);

    match model {
        "quectel_ep06" => {
            let conn = base_port.clone()
                .path("/dev/ttyUSB3")
                .baud_rate(9600)
                .timeout(Duration::from_millis(5000));

            Box::new(QuectelEp06 { connection: conn })
        }
        _ => std::process::exit(1),
    }
}
