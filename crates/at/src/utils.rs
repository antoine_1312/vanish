use serialport::SerialPortBuilder;

pub fn send_serial_command(conn: SerialPortBuilder, command: String) -> Result<String, String> {
    println!("Sending command \"{}\"", command);
    // Receive buffer.
    let mut buf = [0u8; 512];

    let mut interface = match conn.open() {
        Ok(i) => i,
        Err(e) => todo!(),
    };

    interface.write(command.as_bytes());

    // Receive on the slave
    let bytes_recvd = interface.read(&mut buf).unwrap();
    match String::from_utf8((&buf[..bytes_recvd]).to_vec()) {
        Ok(res) => Ok(res),
        Err(_) => todo!(),
    }
}